import { PALETTE } from "@zendeskgarden/react-theming";


const themePalette = {...PALETTE,
    "primary-gray" : "#DBCEFF",
    "hed-text-gray": "#9E9E9E",
    "mes-text-gray": "#5A5A5A",
    "num-text-gray": "#888",
    "list-hover": "#D0FFD861",
    "button-white": "#FFF",
    "primary-white": "#FFFFFF",
    "message-green" : "#68C682",
    'primary-blue': "#6AAFFF"
}

export default themePalette;